# GitLab 🦊 features

> create `.cvs, .json, .html` files of the GitLab features per edition

## Run it

```
npm start
```

## Links

- [https://tanuki-tools.gitlab.io/gitlab-features/](https://tanuki-tools.gitlab.io/gitlab-features/)
- [https://tanuki-tools.gitlab.io/gitlab-features/premium_and_above-features.html](https://tanuki-tools.gitlab.io/gitlab-features/premium_and_above-features.html)
- [https://tanuki-tools.gitlab.io/gitlab-features/ultimate_only-features.html](https://tanuki-tools.gitlab.io/gitlab-features/ultimate_only-features.html)
- [https://tanuki-tools.gitlab.io/gitlab-features/kubernetes-features.html](https://tanuki-tools.gitlab.io/gitlab-features/kubernetes-features.html)
- [https://tanuki-tools.gitlab.io/gitlab-features/monitoring-features.html](https://tanuki-tools.gitlab.io/gitlab-features/monitoring-features.html)


