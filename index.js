const axios = require("axios")
const yaml = require("js-yaml")
const fs = require("fs")
const jsonexport = require('jsonexport')

axios({
  method: "GET",
  url: "https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/features.yml"
}).then(response => response.data)
  .then(data => {
    try {

      const content = yaml.safeLoad(data, {json:true})
      // options: {json:true}
      // If true, then duplicate keys in a mapping will override values rather than throwing an error

      fs.writeFileSync(`./all-features.json`, JSON.stringify(content.features))
      
      /* transformation */
      let mutation = feature => {
        var category1, category2
        if(feature.category) {
          category1 = feature.category[0] != undefined ? feature.category[0] : ""
          category2 = feature.category[1] != undefined ? feature.category[1] : ""

          category1 = category1 === "unknown" ? "" : category1

        } else {
          category1 = ""
          category2 = ""
        }

        return {
          feature: feature.title,
          description: feature.description,
          link_description: feature.link_description != undefined ? feature.link_description : "",
          link: feature.link,
          core: feature.gitlab_core ? "x" : "",
          starter: feature.gitlab_starter ? "x" : "",
          premium: feature.gitlab_premium ? "x" : "",
          ultimate: feature.gitlab_ultimate ? "x" : "",
          saas: feature.gitlab_com ? "x" : "",
          solution: feature.solution != undefined ? feature.solution : "",
          category1: category1,
          category2: category2
        }
      }

      /* filters */
      let core = feature => 
        feature.gitlab_core && 
        feature.gitlab_starter &&
        feature.gitlab_premium &&
        feature.gitlab_ultimate

      let starter = feature => 
        !feature.gitlab_core && 
        feature.gitlab_starter &&
        feature.gitlab_premium &&
        feature.gitlab_ultimate

      let premium = feature => 
        !feature.gitlab_core && 
        !feature.gitlab_starter &&
        feature.gitlab_premium &&
        feature.gitlab_ultimate

      let ultimate = feature => 
        !feature.gitlab_core && 
        !feature.gitlab_starter &&
        !feature.gitlab_premium &&
        feature.gitlab_ultimate


      let core_and_above = content.features
        .filter(core)
        .map(mutation)

      let starter_and_above = content.features
        .filter(starter)
        .map(mutation)

      let premium_and_above = content.features
        .filter(premium)
        .map(mutation)

      let ultimate_only = content.features
        .filter(ultimate)
        .map(mutation)
      
      /*==== Extract kubernetes features ====*/
      let kubernetes_features = content.features
        .filter(
          feature => {
            if(feature.category1==undefined) {feature.category1=""}
            return feature.category1.includes("kubernetes_management") || 
            feature.description.includes("kubernetes") ||
            feature.description.includes("Kubernetes") ||
            feature.title.includes("kubernetes") ||
            feature.title.includes("Kubernetes")
          }
        )
      
      k8s_core_and_above = kubernetes_features.filter(core).map(mutation)
      k8s_starter_and_above = kubernetes_features.filter(starter).map(mutation)
      k8s_premium_and_above = kubernetes_features.filter(premium).map(mutation)
      k8s_ultimate_only  = kubernetes_features.filter(ultimate).map(mutation)

      let kubernetes = k8s_core_and_above.concat(k8s_starter_and_above, k8s_premium_and_above, k8s_ultimate_only)
      
      /*==== Extract monitoring features ====*/
      // monitoring, dashboard
      let monitoring_features = content.features
        .filter(
          feature => {
            return feature.description.includes("monitor") || 
            feature.description.includes("Monitor") ||
            feature.title.includes("monitor") ||
            feature.title.includes("Monitor") ||
            feature.description.includes("dashboard") || 
            feature.description.includes("Dashboard") ||
            feature.title.includes("dashboard") ||
            feature.title.includes("Dashboard")
          }
        )
 
      //console.log(monitoring_features)
      
      
      monitoring_core_and_above = monitoring_features.filter(core).map(mutation)
      monitoring_starter_and_above = monitoring_features.filter(starter).map(mutation)
      monitoring_premium_and_above = monitoring_features.filter(premium).map(mutation)
      monitoring_ultimate_only  = monitoring_features.filter(ultimate).map(mutation)

      let monitoring = monitoring_core_and_above.concat(monitoring_starter_and_above, monitoring_premium_and_above, monitoring_ultimate_only)
      
      /*==== Extract CI/CD features ====*/
      //console.log(content.features)
      let ci_cd_features = content.features.map(mutation)
            .filter(
              feature => {
                if(feature.category1==undefined) {
                  feature.category1=""
                }
                
                return feature.category1.includes("continuous_integration") || 
                feature.category1.includes("continuous_delivery") ||
                feature.category1.includes("continuous delivery")
                //feature.title.includes("continuous integration")
              }
            )
      
      jsonexport(core_and_above, {rowDelimiter: ';'}, function(err, csv){
        if(err) return console.log(err)
        fs.writeFileSync(`./core_and_above-features.csv`, csv)
      })

      jsonexport(starter_and_above, {rowDelimiter: ';'}, function(err, csv){
        if(err) return console.log(err)
        fs.writeFileSync(`./starter_and_above-features.csv`, csv)
      })

      jsonexport(premium_and_above, {rowDelimiter: ';'}, function(err, csv){
        if(err) return console.log(err)
        fs.writeFileSync(`./premium_and_above-features.csv`, csv)
      })

      jsonexport(ultimate_only, {rowDelimiter: ';'}, function(err, csv){
        if(err) return console.log(err)
        fs.writeFileSync(`./ultimate_only-features.csv`, csv)
      })

      let all_tiers = core_and_above.concat(starter_and_above, premium_and_above, ultimate_only)

      jsonexport(all_tiers, {rowDelimiter: ';'}, function(err, csv){
        if(err) return console.log(err)
        fs.writeFileSync(`./all-features.csv`, csv)
      })

      let html_report = (title, features) => `
      <style>
        .center {
          text-align: center;
        }

        th {
          position: -webkit-sticky;
          position: sticky;
          top: 0;
          z-index: 2;
        }

        #features {
          font-family: Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        
        #features td, #features th {
          border: 1px solid #ddd;
          padding: 8px;
        }
        
        #features tr:nth-child(even){background-color: #f2f2f2;}
        
        #features tr:hover {background-color: #ddd;}
        
        #features th {
          padding-top: 12px;
          padding-bottom: 12px;
          text-align: left;
          background-color: #6d49cb;
          color: white;
        }

      </style>
      <h2 style="font-family: Arial, Helvetica, sans-serif;">${title}</h2>
      <hr>
      <table id="features">
        <thead>
          <tr>
            <th>id</th>
            <th>feature</th>
            <th>description</th>
            <th>link</th>
            <th>core</th>
            <th>starter</th>
            <th>premium</th>
            <th>ultimate</th>
            <th>saas</th>
            <th>solution</th>
            <th>category1</th>
            <th>category2</th>
          </tr>
        </thead>
        <tbody>
          ${features.map((feature, index) => 
            `
              <tr>
                <td>${index+1}</td>
                <td>${feature.feature}</td>
                <td>${feature.description}</td>
                <td><a href="${feature.link}">${feature.link_description}</a></td>
                <td class="center"><b>${feature.core}</b></td>
                <td class="center"><b>${feature.starter}</b></td>
                <td class="center"><b>${feature.premium}</b></td>
                <td class="center"><b>${feature.ultimate}</b></td>
                <td class="center"><b>${feature.saas}</b></td>
                <td class="center"><b>${feature.solution}</b></td>
                <td>${feature.category1}</td>
                <td>${feature.category2}</td>
              </tr>
            `
          ).join("")}
        </tbody>
      </table>
    `

    fs.writeFileSync(`./core_and_above-features.html`, html_report("Core and above", core_and_above))
    fs.writeFileSync(`./starter_and_above-features.html`, html_report("Starter and above", starter_and_above))
    fs.writeFileSync(`./premium_and_above-features.html`, html_report("Premium and above", premium_and_above))
    fs.writeFileSync(`./ultimate_only-features.html`, html_report("Ultimate", ultimate_only))
    fs.writeFileSync(`./all-features.html`, html_report("All tiers", all_tiers))
    
    fs.writeFileSync(`./kubernetes-features.html`, html_report("Kubernetes", kubernetes))
    
    fs.writeFileSync(`./monitoring-features.html`, html_report("Monitoring", monitoring))
    
    fs.writeFileSync(`./ci-cd-features.html`, html_report("CI/CD", ci_cd_features))




    } catch(error) {
      console.log(`😡`, error.name, error.message)
    }   
  })
  .catch(error => {
    console.log(`😡`, error.name, error.message)
  })